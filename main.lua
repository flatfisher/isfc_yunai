display.setStatusBar(display.HiddenStatusBar)

width = display.contentWidth
print(width)
height = display.contentHeight
print(height)

-- 追加 6/27
function main()

local displayItem = display.newGroup();
local backGround = display.newImageRect( displayItem,"yunai_bg.png",width, height)
    backGround.x = width/2;backGround.y = height/2

local physics = require("physics")
physics.start()

function onAccelerometer(event)
     x = event.xGravity
     y = event.xGravity
     physics.setGravity(9.8*x, -9.8*y)

end

Runtime:addEventListener("accelerometer",onAccelerometer)

-- function clear(event) ~ endを追加 6/27
function clear( event )
	local i
	for i = displayItem.numChildren,1,-1 do
		local child = displayItem[i]
		child.parent:remove(child)
		child = nil
	end
end

--displayItem, を追加　6/27

local wall1 = display.newRect(displayItem,0,0,width,5)
physics.addBody(wall1,"static")

local wall2 = display.newRect(displayItem,0,height,width,5)
physics.addBody(wall2,"static")

local wall3 = display.newRect(displayItem,0,0,5,height)
physics.addBody(wall3,"static")

local wall4 = display.newRect(displayItem,width-5,0,5,height)
physics.addBody(wall4,"static")

local wall5 = display.newRect(displayItem,width/2,height/3,width/25,height/2)
physics.addBody(wall5,"static")

local wall6 = display.newRect(displayItem,width/5,height/2,width/27,height/2)
physics.addBody(wall6,"static")

local wall7 = display.newRect(displayItem,width/3,height/9,width/25,height/2)
physics.addBody(wall7,"static")

local wall8 = display.newRect(displayItem,width/27,height/4,width/25,height/2)
physics.addBody(wall8,"static")

local wall9 = display.newRect(displayItem,width/20,height/5,width/2,height/30)
physics.addBody(wall9,"static")

local wall10 = display.newRect(displayItem,width/2,height/2,width/2,height/30)
physics.addBody(wall10,"static")

local wall11 = display.newRect(displayItem,width/10,height/4,width/2,height/30)
physics.addBody(wall11,"static")

local wall12 = display.newRect(displayItem,width/7,height/15,width/2,height/30)
physics.addBody(wall12,"static")

local ball = display.newCircle(displayItem,20,20,10)
physics.addBody(ball,{density =   0.5 , friction =  1.0 , bounce = 0.7})
ball:setFillColor(0,204,255)

local goalPotision = display.newRect(displayItem,width-300,height-80,50,50);
physics.addBody(goalPotision , "static",{isSensor = true } )
goalPotision:setFillColor(0,51,255)

function endstage()
	clear()
	goalPotision:removeEventListener( "collision",onCollision )
	Runtime:removeEventListener("accelerometer",onAccelerometer)
	physics.stop()
	main()
end

function onCollision( event )

     if( event.phase == "began" ) then

     -- local text = display.newText("GOAL!!",width/2,height/2,native.system)を削除
     timer.performWithDelay( 100, endstage, 1 )

     end

     end

goalPotision:addEventListener( "collision",onCollision )

--追加　6/27
end
main()
